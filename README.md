Commerce Paga Mas Tarde
=================

The extension provides Paga Más Tarde ( https://www.pagamastarde.com/ ) payment integration for DrupalCommerce which enables payment processing using financing.


## Requirements

To use the module you need a free Paga+Rtarde account: https://bo.pagamastarde.com/
## Setup

1. Install the Easy Breadcrumb module as you would normally install a contributed
   Drupal module.
   
