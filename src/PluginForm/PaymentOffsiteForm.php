<?php

namespace Drupal\commerce_pagamastarde\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Custom implementation of the BasePaymentOffSiteForm.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * Service Endpoint.
   *
   * @var string
   */
  private static $fromActionUrl = 'https://pmt.pagantis.com/v1/installments';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);
    $urlCallback = Url::fromRoute('commerce_payment.notify',
      ['commerce_payment_gateway' => 'paga_mas_tarde'],
      ['absolute' => TRUE])
      ->toString();

    $submitData = [
      'order_id' => $this->getOrder()->id(),
      'callback_url' => $urlCallback,
      'cancelled_url' => $form['#cancel_url'],
      'ok_url' => $form['#return_url'],
      'nok_url' => $form['#cancel_url'],
      'amount' => number_format($this->getPayment()->getAmount()->getNumber(), 2, '', ''),
      'currency' => $this->getPayment()->getAmount()->getCurrencyCode(),
      'account_id' => $this->getApiKeys()['public'],
      'discount[full]' => 'false',
      // 'discount[full]' Will only accept false.
      'email' => substr($this->getOrder()->getEmail(), 0, 255),
      'description' => $this->getOrderDescription(),
      'full_name' => substr($this->getFromBilling('given_name'), 0, 50)
      . " " . substr($this->getFromBilling('family_name'), 0, 50),
      'address[street]' => substr($this->getFromBilling('address_line1'), 0, 60),
      'address[city]' => substr($this->getFromBilling('locality'), 0, 40),
      'address[province]' => substr($this->getFromBilling('administrative_area'), 0, 40),
      'address[zipcode]' => substr($this->getFromBilling('postal_code'), 0, 20),
      'shipping[street]' => substr($this->getFromBilling('address_line1'), 0, 60),
      'shipping[city]' => substr($this->getFromBilling('locality'), 0, 40),
      'shipping[province]' => substr($this->getFromBilling('administrative_area'), 0, 40),
      'shipping[zipcode]' => substr($this->getFromBilling('postal_code'), 0, 20),
    ];
    $this->sign($submitData);
    $submitData = array_merge($submitData, $this->getOrderItems());
    $form = $this->buildRedirectForm($form, $form_state, self::$fromActionUrl, $submitData, 'post');
    $form['#attached']['library'][] = 'commerce_pagamastarde/cdn_pagamastarde';
    return $form;
  }

  /**
   * Gets the billing information.
   *
   * @param string $key
   *   The address billing key.
   *
   * @return mixed
   *   Returns the key from the address.
   */
  protected function getFromBilling($key) {
    static $address = NULL;

    if (empty($address)) {
      $address = $this->getOrder()
        ->getBillingProfile()
        ->address
        ->first();
    }

    return $address->$key;
  }

  /**
   * Gets the order description for the request payload.
   *
   * @return bool|string
   *   Returns the shipping description.
   */
  protected function getOrderDescription() {
    $order = $this->getOrder();
    $description = [];

    foreach ($order->getItems() as $item) {
      if ($item->getPurchasedEntity()->getEntityType() !== 'shipping') {
        $description[] = $item->getTitle() . " (" . $item->getQuantity() . ")";
      }
    }

    return substr(implode(', ', $description), 0, 255);
  }

  /**
   * Gets the order items as array to be added in the payload to pagamastarde.
   *
   * @return array
   *   Returns the order items as an array.
   */
  protected function getOrderItems() {
    $order = $this->getOrder();
    $items = [];
    $i = 1;

    foreach ($order->getItems() as $item) {

      if ($item->getPurchasedEntity()->getEntityType() !== 'shipping') {
        $items["items[$i][description]"] = $item->getTitle() . " (" . $item->getQuantity() . ")";
        $items["items[$i][quantity]"] = $item->getQuantity();
        $items["items[$i][amount]"] = number_format($item->getTotalPrice()->getNumber(), 2, ',', '');
        $i++;
      }

      // Shipping.
      if ($item->getPurchasedEntity()->getEntityType() == "shipping") {
        $items["items[0][description]"] = $item->getTitle();
        $items["items[0][quantity]"] = 1;
        $items["items[0][amount]"] = $item->getTotalPrice()->getNumber();
      }
    }

    return $items;
  }

  /**
   * Gets the Order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   Returns the payment Order.
   */
  protected function getOrder() {
    return $this->getPayment()->getOrder();
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface
   *   Returns the payment entity.
   */
  protected function getPayment() {
    return $this->entity;
  }

  /**
   * Gets the Payment Gateway.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface|null
   *   Returns the payment interface.
   */
  protected function getPaymentGateway() {
    return $this->getPayment()->getPaymentGateway();
  }

  /**
   * Gets the gateway config values.
   *
   * @return array
   *   Returns the gateway config array.
   */
  protected function getGatewayConfig() {
    return $this->getPaymentGateway()->getPlugin()->getConfiguration();
  }

  /**
   * Signs the payload for the request to paga mas tarde.
   *
   * @param array $data
   *   The hashed signature.
   *
   * @return array
   *   Returns the signature for the request.
   */
  protected function sign(array &$data) {
    // Create the MD5 hash fingerprint.
    $private = $this->getApiKeys()['private'];
    $public = $this->getApiKeys()['public'];
    $amount = $data['amount'];
    $okUrl = $data['ok_url'];
    $currency = $data['currency'];
    $nokUrl = $data['nok_url'];
    $callbackUrl = $data['callback_url'];
    $cancelledUrl = $data['cancelled_url'];
    $discount = 'false';
    $orderId = $data['order_id'];

    $message = $private . $public . $orderId . $amount . $currency .
      $okUrl . $nokUrl . $callbackUrl . $discount . $cancelledUrl;

    $data['signature'] = hash('sha512', $message);
    return $data;
  }

  /**
   * Gets the API keys from the config depending on the selected environment.
   *
   * @return array
   *   Returns the API keys.
   */
  protected function getApiKeys() {
    $config = $this->getGatewayConfig();
    return [
      'private' => $config[$config['mode'] . '_private_key'],
      'public' => $config[$config['mode'] . '_public_key'],
    ];

  }

}