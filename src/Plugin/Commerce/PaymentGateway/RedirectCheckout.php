<?php

namespace Drupal\commerce_pagamastarde\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides the PagaMasTarde offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "pagamastarde_redirect_checkout",
 *   label = @Translation("Pagamastarde (Redirect to Paga Mas Tarde)"),
 *   display_label = @Translation("Paga Mas Tarde"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_pagamastarde\PluginForm\PaymentOffsiteForm",
 *   },
 *   payment_type = "payment_default"
 * )
 */
class RedirectCheckout extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'prod_public_key' => '',
      'prod_private_key' => '',
      'test_public_key' => '',
      'test_private_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['test_public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test public key'),
      '#default_value' => $this->configuration['test_public_key'],
      '#required' => TRUE,
    ];

    $form['test_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test private key'),
      '#default_value' => $this->configuration['test_private_key'],
      '#required' => TRUE,
    ];

    $form['prod_public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production public key'),
      '#default_value' => $this->configuration['prod_public_key'],
      '#required' => TRUE,
    ];

    $form['prod_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production private key'),
      '#default_value' => $this->configuration['prod_private_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['test_public_key'] = $values['test_public_key'];
    $this->configuration['test_private_key'] = $values['test_private_key'];
    $this->configuration['prod_public_key'] = $values['prod_public_key'];
    $this->configuration['prod_private_key'] = $values['prod_private_key'];
  }

  /**
   * {@inheritdoc}
   *
   * Create a payment with state NEW, that will be updated once a notification is received.
   */
  public function onReturn(OrderInterface $order, Request $request) {

    try {
      $paymentData = [
        'amount'          => $order->getTotalPaid(),
        'payment_gateway' => $this->entityId,
        'order_id'        => $order->id(),
        'test'            => $this->getMode() == 'test',
        'remote_id'       => '',
        'remote_state'    => 'New',
        'state'           => 'New',
        'authorized'      => $this->time->getRequestTime(),
      ];
  
      $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment        = $paymentStorage->loadByProperties(['order_id' => $order->id()]);

      // Check that there is no payment already related to this order.
      if (count($payment) > 0) {
        throw new \Exception("Error order already paid");
      }

      $payment = $paymentStorage->create($paymentData);

      $payment->save();
    }
    catch (\Exception $exception) {
      // Return empty response with 500 error code.
      return new JsonResponse($exception->getMessage(), 500);
    }

    // Return empty response with 200 status code.
    return new JsonResponse();

  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);
    $input = $request->getContent();
    $notification = json_decode($input, TRUE);

    try {
      $this->checkSignatures($notification);

      $orderId = $notification['data']['order_id'];

      $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment        = $paymentStorage->loadByProperties(['order_id' => $orderId]);

      // Check if the order has a payment.
      if (count($payment) == 0) {
        throw new \Exception("Error no payment registered for this order");
      }
      
      // Update the payment attributes.
      $payment->setAmount(new Price($notification['data']['amount'] / 100, 'EUR'));
      $payment->setAuthorizedTime($this->time->getRequestTime());
      $payment->set('remote_id', $notification['data']['id']);
      $payment->set('remote_state', $notification['data']['status']);
      $payment->set('test', $this->getMode() == 'test');

      if (isset($notification['event']) && $notification['event'] == 'charge.created') {
        $payment->setState('Completed');
        $payment->save();
      }
    }

    catch (\Exception $exception) {
      // Return empty response with 500 error code.
      return new JsonResponse($exception->getMessage(), 500);
    }
    // Return empty response with 200 status code.
    return new JsonResponse();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkSignatures($notification) {
    $signatureCheck = sha1($this->getApiKeys()['private'] .
      $notification['account_id'] .
      $notification['api_version'] .
      $notification['event'] .
      $notification['data']['id']);

    $signatureCheckSha512 = hash('sha512',
    $this->getApiKeys()['private'] .
      $notification['account_id'] .
      $notification['api_version'] .
      $notification['event'] .
      $notification['data']['id']);

    if (($signatureCheck != $notification['signature'])
      && ($signatureCheckSha512 != $notification['signature'])) {
      throw new \Exception('Not valid signatures');
    }
  }

  /**
   * Gets the API keys from the config depending on the environment selected.
   */
  protected function getApiKeys() {
    $config = $this->configuration;
    return [
      'private' => $config[$config['mode'] . '_private_key'],
      'public'  => $config[$config['mode'] . '_public_key'],
    ];
  }

}
